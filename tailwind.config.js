/** @type {import('tailwindcss').Config} */
// export default {
//   content: ["./index.html", "./src/**/*.{js,ts,jsx,tsx}"],
//   plugins: [],
// };

export default {
  content: ["./index.html", "./src/**/*.{js,ts,jsx,tsx}"],
  theme: {
    extend: {},
    colors: {
      white: {
        100: "#ffffff",
        40: "rgba(255, 255, 255, 0.4)",
      },
      bgwhite: "#F1F1F1",
      // softGray: "rgba(107, 114, 128, var(--tw-bg-opacity))",

      black: {
        100: "#000000",
        50: "rgba(0, 0, 0, 0.5)",
      },
      gray: {
        100: "#808080",
        50: "rgba(0, 0, 0, 0.5)",
      },
      orange: {
        1: "#FCB64E",
        2: "#FF6600",
      },
    },
  },
  plugins: [
    // eslint-disable-next-line no-undef
    require("daisyui"),
    // eslint-disable-next-line no-undef
    require("tailwindcss-animate"),
    // daisyui,
    // eslint-disable-next-line no-undef
    // require("@tailwindcss/typography")
  ],
};
