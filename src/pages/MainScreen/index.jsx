import { useState, useEffect } from "react";
import Navbar from "../../components/Navbar";
import Dropdown from "../../components/Dropdown";
import Banner from "../../components/Banner";
import { DefaultPagination } from "../../components/DefaultPagination";
import { fetchData } from "../../api/api";
import { extractFirstImageUrl } from "../../utils/extractFirstImageUrl";
import { formatDate } from "../../utils/formateDate";
import Card from "../../components/Card";

export default function MainScreen() {
  return <Screen />;
}

const Screen = () => {
  const [ideasData, setIdeasData] = useState(null);
  const [error, setError] = useState(null);
  const [currentPage, setCurrentPage] = useState(1);
  const [pageSize, setPageSize] = useState(10);
  const [selectedValue, setSelectedValue] = useState("10");
  const [sortOption, setSortOption] = useState("Newest");
  const [totalItems, setTotalItems] = useState(null);

  useEffect(() => {
    const fetchDataFromApi = async () => {
      try {
        const { data, totalItems } = await fetchData(currentPage, pageSize);
        setIdeasData(data);
        setTotalItems(totalItems);
      } catch (error) {
        setError(error.message);
      }
    };

    fetchDataFromApi();
  }, [currentPage, pageSize, sortOption]); 

  useEffect(() => {
    setPageSize(parseInt(selectedValue));
  }, [selectedValue]);

  const scrollToDiv = (id) => {
    const element = document.getElementById(id);
    if (element) {
      element.scrollIntoView({ behavior: "smooth", block: "start" });
    }
  };

  const handlePageChange = (pageNumber) => {
    setCurrentPage(pageNumber);
  };

  const handleSelectOption = (option) => {
    setSelectedValue(option);
  };

  const handleSelectOption2 = (option) => {
    setSortOption(option);
  };

  const totalPages = Math.ceil(totalItems / pageSize);

  return (
    <div className="bg-white-100 relative pb-10">
      <Navbar scrollToDiv={scrollToDiv} />
      <Banner />
      <div className="mx-20">
        <div className="flex flex-row justify-between py-25">
          <div>
            {" "}
            Showing {(currentPage - 1) * pageSize + 1}-
            {Math.min(currentPage * pageSize, totalItems || 0)} of {totalItems}
          </div>
          <div className="flex flex-row justify-end">
            <div className="flex flex-row pr-10">
              {" "}
              <p> Show per page : </p>
              <Dropdown
                options={["10", "20", "50"]}
                defaultOption={selectedValue}
                onSelect={handleSelectOption}
              />
            </div>
            <div className="flex flex-row">
              {" "}
              <p> Sort by : </p>
              <Dropdown
                options={["Newest", "Oldest"]}
                defaultOption={sortOption}
                onSelect={handleSelectOption2}
              />
            </div>
          </div>
        </div>
        <div className="ideas-container flex flex-wrap justify-center pt-10">
          {error && <p>{error}</p>}
          {ideasData &&
            ideasData.data &&
            ideasData.data
              .slice()
              .sort((a, b) => {
                if (sortOption === "Newest") {
                  return new Date(b.published_at) - new Date(a.published_at);
                } else {
                  return new Date(a.published_at) - new Date(b.published_at);
                }
              })
              .map((idea) => (
                <div key={idea.id} className="pr-3 py-5 pt">
                  <Card
                    title={idea.title}
                    date={formatDate(idea.published_at)}
                    img={extractFirstImageUrl(idea.content)}
                  />
                </div>
              ))}
        </div>
        <div className="flex justify-center mt-10">
          <DefaultPagination
            nextFunction={() => handlePageChange(currentPage + 1)}
            prevFunction={() => handlePageChange(currentPage - 1)}
            currentPage={currentPage}
            handlePageChange={setCurrentPage} 
            totalPages={totalPages} 
          />
        </div>
      </div>
    </div>
  );
};
