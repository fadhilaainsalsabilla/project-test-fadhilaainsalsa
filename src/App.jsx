import "./App.css";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import MainScreen from "./pages/MainScreen/index";
import "aos/dist/aos.css";

function App() {
  return (
    <Router>
      <div>
        <MainScreen />
      </div>
      <Routes>
        <Route path="/" element={""} />
      </Routes>
    </Router>
  );
}

export default App;
