import dataNotFound from "../assets/dataNotFound.jpg";

export const extractFirstImageUrl = (content) => {
  const regex = /<img.*?src="(.*?)"/;
  const match = content.match(regex);
  return match ? match[1] : dataNotFound;
};
