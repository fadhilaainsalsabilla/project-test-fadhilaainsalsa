export const fetchData = async (pageNumber, pageSize) => {
  try {
    const response = await fetch(
      `https://suitmedia-backend.suitdev.com/api/ideas?page[number]=${pageNumber}&page[size]=${pageSize}`,
      {
        headers: {
          Accept: "application/json",
        },
      }
    );

    if (!response.ok) {
      throw new Error("Network response was not ok");
    }

    const data = await response.json();
    const totalItems = data?.meta?.total || 0; // Mendapatkan total data dari meta
    // console.log(totalItems);

    return { data, totalItems }; // Mengembalikan data dan totalItems
  } catch (error) {
    throw new Error(error.message);
  }
};
