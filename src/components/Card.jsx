const Card = ({ title, img, date }) => {
  return (
    <div className="card bg-base-100 shadow-xl flex">
      <figure className="w-64 h-48">
        <img
          src={img}
          alt=""
          className="w-64 h-48 object-cover"
          loading="lazy" 
        />
      </figure>
      <div className="card-body w-64 h-48">
        <p className="font-bold text-gray-100">{date}</p>
        <p
          className="card-title"
          style={{
            display: "-webkit-box",
            WebkitBoxOrient: "vertical",
            WebkitLineClamp: 3,
            overflow: "hidden",
            textOverflow: "ellipsis",
          }}
        >
          {title}
        </p>
      </div>
    </div>
  );
};

export default Card;
