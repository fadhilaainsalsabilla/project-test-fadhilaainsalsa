import { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import menuicon from "../assets/menuicon.png";
import companyIcon from "../assets/site-logo.png";

const Navbar = ({ scrollToDiv }) => {
  const [prevScrollPos, setPrevScrollPos] = useState(0);
  const [visible, setVisible] = useState(true);
  const [isScrollUp, setIsScrollUp] = useState(false);

  useEffect(() => {
    const handleScroll = () => {
      const currentScrollPos = window.pageYOffset;
      setVisible(prevScrollPos > currentScrollPos || currentScrollPos < 10);
      setIsScrollUp(currentScrollPos < prevScrollPos);
      setPrevScrollPos(currentScrollPos);
    };

    window.addEventListener("scroll", handleScroll);

    return () => window.removeEventListener("scroll", handleScroll);
  }, [prevScrollPos, visible]);

  return (
    <div
      className={`navbar bg-orange-2 backdrop-blur-sm px-4 md:px-8 lg:px-16 sticky top-0 z-10 transition-all duration-300 ${
        visible ? "" : "opacity-0"
      }`}
      style={{
        backgroundColor: isScrollUp
          ? "rgba(255, 102, 0, 0.8)"
          : "rgba(255, 102, 0, 1)",
      }}
    >
      <div className="md:navbar-center md:navbar-start">
        <div className="dropdown">
          <div tabIndex={0} role="button" className="lg:hidden">
            <div className="mr-2">
              <img src={menuicon} alt="" className="h-7 w-7" />
            </div>
          </div>
          <ul
            tabIndex={0}
            className="menu menu-sm dropdown-content mt-3 z-[1] p-2 shadow bg-base-100 rounded-box w-52"
          >
            <li onClick={() => scrollToDiv("")}>
              <Link to="/" style={{ textDecoration: "none", color: "inherit" }}>
                Work
              </Link>
            </li>
            <li onClick={() => scrollToDiv("")}>
              <Link
                to="/"
                style={{ textDecoration: "none", color: "inherit" }}
                className="menu-item-hover"
              >
                About
              </Link>
            </li>{" "}
            <li onClick={() => scrollToDiv("")}>
              <Link to="/" style={{ textDecoration: "none", color: "inherit" }}>
                Services
              </Link>
            </li>
            <li onClick={() => scrollToDiv("")}>
              <Link to="/" style={{ textDecoration: "none", color: "inherit" }}>
                Ideas
              </Link>
            </li>
            <li onClick={() => scrollToDiv("")}>
              <Link to="/" style={{ textDecoration: "none", color: "inherit" }}>
                Career
              </Link>
            </li>
            <li onClick={() => scrollToDiv("")}>
              <Link to="/" style={{ textDecoration: "none", color: "inherit" }}>
                Contact
              </Link>
            </li>
          </ul>
        </div>
        <img src={companyIcon} alt="Company Logo" className="w-24" />
      </div>
      <div className="navbar-end hidden lg:flex">
        <ul className="menu menu-horizontal px-1  text-white-100">
          <li
            onClick={() => scrollToDiv("about")}
            className="transition transform hover:-translate-y-1 motion-reduce:transition-none motion-reduce:hover:transform-none ... "
          >
            <Link to="/" style={{ textDecoration: "none", color: "inherit" }}>
              Work
            </Link>
          </li>
          <li
            onClick={() => scrollToDiv("")}
            className="transition transform hover:-translate-y-1 motion-reduce:transition-none motion-reduce:hover:transform-none ... "
          >
            <Link
              to="/"
              style={{ textDecoration: "none", color: "inherit" }}
              className="menu-item-hover"
            >
              About
            </Link>
          </li>{" "}
          <li
            onClick={() => scrollToDiv("")}
            className="transition transform hover:-translate-y-1 motion-reduce:transition-none motion-reduce:hover:transform-none ... "
          >
            <Link
              to="/projects"
              style={{ textDecoration: "none", color: "inherit" }}
            >
              Projects
            </Link>
          </li>
          <li
            onClick={() => scrollToDiv("")}
            className="transition transform hover:-translate-y-1 motion-reduce:transition-none motion-reduce:hover:transform-none ... "
          >
            <Link to="/" style={{ textDecoration: "none", color: "inherit" }}>
              Services
            </Link>
          </li>
          <li
            onClick={() => scrollToDiv("")}
            className="transition transform hover:-translate-y-1 motion-reduce:transition-none motion-reduce:hover:transform-none ... "
          >
            <Link to="/" style={{ textDecoration: "none", color: "inherit" }}>
              Ideas
            </Link>
          </li>
          <li
            onClick={() => scrollToDiv("")}
            className="transition transform hover:-translate-y-1 motion-reduce:transition-none motion-reduce:hover:transform-none ... "
          >
            <Link to="/" style={{ textDecoration: "none", color: "inherit" }}>
              Careers
            </Link>
          </li>
          <li
            onClick={() => scrollToDiv("")}
            className="transition transform hover:-translate-y-1 motion-reduce:transition-none motion-reduce:hover:transform-none ... "
          >
            <Link to="/" style={{ textDecoration: "none", color: "inherit" }}>
              Contact
            </Link>
          </li>
        </ul>
      </div>
    </div>
  );
};

export default Navbar;
