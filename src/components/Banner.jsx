import banner from "../assets/banner.png";
import { useState, useEffect } from "react";

const Banner = () => {
  const [scrollY, setScrollY] = useState(0);

  useEffect(() => {
    const handleScroll = () => {
      setScrollY(window.scrollY);
    };

    window.addEventListener("scroll", handleScroll);

    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, []);

  return (
    <div className="banner-container relative">
      <img
        src={banner}
        alt="Banner"
        className="banner-image w-full transform -skew-y-6"
        style={{ position: "relative", top: "-130px" }}
      />
      <div
        className="absolute inset-x-0 top-1/4 transform -translate-y-1/2 w-full text-center"
        style={{ transform: `translateY(${scrollY * 0.5}px)` }}
      >
        <h1 className="font-bold text-white-100 text-4xl">IDEAS</h1>
        <h1 className="text-white-100 text-xl">
          {" "}
          Where all our great things begin
        </h1>
      </div>
    </div>
  );
};

export default Banner;
