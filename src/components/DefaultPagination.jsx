import React from "react";
import { Button, IconButton } from "@material-tailwind/react";
import {
  ChevronDoubleRightIcon,
  ChevronDoubleLeftIcon,
} from "@heroicons/react/24/solid";

import { ChevronRightIcon, ChevronLeftIcon } from "@heroicons/react/24/solid";

export function DefaultPagination({
  nextFunction,
  prevFunction,
  handlePageChange,
  currentPage,
  totalPages,
}) {
  const [active, setActive] = React.useState(currentPage);

  React.useEffect(() => {
    setActive(currentPage);
  }, [currentPage]);

  const getPageNumbers = () => {
    const pageNumbers = [];
    const maxPagesToShow = 5;
    let startPage = 1;
    let endPage = totalPages;

    if (totalPages > maxPagesToShow) {
      startPage = Math.max(active - Math.floor(maxPagesToShow / 2), 1);
      endPage = startPage + maxPagesToShow - 1;

      if (endPage > totalPages) {
        endPage = totalPages;
        startPage = endPage - maxPagesToShow + 1;
      }
    }

    for (let i = startPage; i <= endPage; i++) {
      pageNumbers.push(i);
    }

    return pageNumbers;
  };

  const getItemProps = (index) => ({
    variant: "text",
    color: "gray",
    style: {
      backgroundColor: active === index ? "#FF6600" : "transparent",
      color: active === index ? "white" : "inherit",
      borderRadius: "0.5rem",
      display: "flex",
      justifyContent: "center",
      padding: "1.5rem",
    },
    onClick: () => {
      setActive(index);
      handlePageChange(index);
    },
  });

  const next = () => {
    if (active === totalPages) return;

    const nextPage = active + 1;
    setActive(nextPage);
    nextFunction();
    handlePageChange(nextPage);
  };

  const prev = () => {
    if (active === 1) return;

    const prevPage = active - 1;
    setActive(prevPage);
    prevFunction();
    handlePageChange(prevPage);
  };

  const goToFirstPage = () => {
    setActive(1);
    handlePageChange(1);
  };

  const goToLastPage = () => {
    setActive(totalPages);
    handlePageChange(totalPages);
  };

  return (
    <div className="flex items-center gap-4">
      <Button
        variant="text"
        className="flex items-center gap-2"
        onClick={goToFirstPage}
        disabled={active === 1}
        style={{ color: active === 1 ? "gray" : "inherit" }}
      >
        <ChevronDoubleLeftIcon
          strokeWidth={2}
          className="h-4 w-4"
          style={{ color: active === totalPages ? "gray" : "inherit" }}
        />
      </Button>
      <Button
        variant="text"
        className="flex items-center gap-2"
        onClick={prev}
        disabled={active === 1}
        style={{ color: active === 1 ? "gray" : "inherit" }}
      >
        <ChevronLeftIcon
          strokeWidth={2}
          className="h-4 w-4"
          style={{ color: active === 1 ? "gray" : "inherit" }}
        />
      </Button>
      <div className="flex items-center gap-2">
        {getPageNumbers().map((pageNumber) => (
          <IconButton key={pageNumber} {...getItemProps(pageNumber)}>
            {pageNumber}
          </IconButton>
        ))}
      </div>
      <Button
        variant="text"
        className="flex items-center gap-2"
        onClick={next}
        disabled={active === totalPages}
        style={{ color: active === totalPages ? "gray" : "inherit" }}
      >
        <ChevronRightIcon
          strokeWidth={2}
          className="h-4 w-4"
          style={{ color: active === totalPages ? "gray" : "inherit" }}
        />
      </Button>
      <Button
        variant="text"
        className="flex items-center gap-2"
        onClick={goToLastPage}
        disabled={active === totalPages}
        style={{ color: active === totalPages ? "gray" : "inherit" }}
      >
        <ChevronDoubleRightIcon
          strokeWidth={2}
          className="h-4 w-4"
          style={{ color: active === totalPages ? "gray" : "inherit" }}
        />
      </Button>
    </div>
  );
}
