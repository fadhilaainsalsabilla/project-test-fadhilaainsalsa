# Suitmedia Frontend Developer Internship Test

Hello! This project serves as an evaluation for frontend developers applying for an internship position at Suitmedia.

The project is built using JavaScript and ReactJS.

# Features

- Explore a curated selection of ideas

- Filter per page ideas by newest or oldest

- Navigate through multiple pages of ideas with pagination

- Retrieve data via API

- Implement lazy loading of images for enhanced performance

# See at

https://project-test-fadhilaainsalsa.vercel.app/
